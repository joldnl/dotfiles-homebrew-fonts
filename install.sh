#!/bin/bash

# ------------------------------------
# Install Homwbrew Fonts
# ------------------------------------

# Install Source Code Pro
echo ""
echo "-------------------------------------------"
echo "| Install Homebrew Fonts ...              |"
echo "-------------------------------------------"

brew tap caskroom/fonts

# Install Source Code Pro
echo "Installing Source Code Pro ..."
brew cask install font-source-code-pro
echo "Done! ..."
echo ""

# Install Nerd Font
echo "Installing Nerd Font ..."
brew cask install font-hack-nerd-font
echo "Done! ..."
echo ""

# Install font-fontawesome Font
echo "Installing Font Awesome ..."
brew cask install font-fontawesome
echo "Done! ..."
echo ""

# Install Fira Code Pro
echo "Installing Fira Code Pro Font ..."
brew cask install font-fira-code
echo "Done! ..."
echo ""
